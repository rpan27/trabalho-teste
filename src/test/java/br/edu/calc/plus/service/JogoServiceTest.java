/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.calc.plus.service;

import br.edu.calc.plus.domain.EOperator;
import br.edu.calc.plus.repo.JogoRepo;
import br.edu.calc.plus.util.Util;
import java.util.Random;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

/**
 *
 * @author danie
 */
@ExtendWith(SpringExtension.class)
@ActiveProfiles("test")
public class JogoServiceTest {

    public JogoServiceTest() {
    }

    @MockBean
    private JogoRepo jDao;

    @MockBean
    private Random random;

    @InjectMocks
    JogoService jogoService;

    @BeforeAll
    public static void setUpClass() {
    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        Mockito.when(jDao.getAllAcertosUser(1)).thenReturn(3L);

    }

    @AfterEach
    public void tearDown() {
    }

    @Test
    public void testBonusInicialSoma() {
        Mockito.when(random.nextDouble()).thenReturn(0.3);
        Mockito.when(jDao.getAllAcertosUser(1)).thenReturn(10l);
        var resposta = jogoService.bonusInicial(EOperator.soma, (1));
        var experado = Util.truncateDecimal(2.80, 2);

        assertEquals(experado, resposta);
    }

    @Test
    public void testBonusInicialSubtracao() {
        Mockito.when(random.nextDouble()).thenReturn(0.3);
        Mockito.when(jDao.getAllAcertosUser(1)).thenReturn(10l);
        var resposta = jogoService.bonusInicial(EOperator.subtracao, (1));
        var experado = Util.truncateDecimal(2.90, 2);

        assertEquals(experado, resposta);
    }

    @Test
    public void testBonusInicialDivisao() {
        Mockito.when(random.nextDouble()).thenReturn(0.3);
        Mockito.when(jDao.getAllAcertosUser(1)).thenReturn(10l);
        var resposta = jogoService.bonusInicial(EOperator.divisao, (1));
        var experado = Util.truncateDecimal(3.59, 2);

        assertEquals(experado, resposta);
    }

    @Test
    public void testBonusInicialMultiplicacao() {
        Mockito.when(random.nextDouble()).thenReturn(0.3);
        Mockito.when(jDao.getAllAcertosUser(1)).thenReturn(10l);
        var resposta = jogoService.bonusInicial(EOperator.multiplicacao, (1));
        var experado = Util.truncateDecimal(3.19, 2);

        assertEquals(experado, resposta);
    }

    @Test
    public void testCriarJogosAleatorios() {
        var resposta = jogoService.criarJogosAleatorio(10, 1);
        
        assertEquals(10,resposta.size());
    }

}
