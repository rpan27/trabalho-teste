/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.calc.plus.domain;

import java.time.LocalDateTime;
import java.util.ArrayList;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.DisplayName;

/**
 *
 * @author danie
 */

public class PartidaTest {
    
    public PartidaTest() {
    }
    
    
    @BeforeAll
    public static void setUpClass() {
         
        //Jogo(Integer idjogo, double valor1, double valor2, EOperator operador, double resultado, double resposta, double bonus)
        
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    @Test
    @DisplayName("Verificar acertos")
    public void testGetAcertos() {
         ArrayList<Jogo> lista = new ArrayList<>();
        lista.add( new Jogo(1,1.0,1.0,EOperator.soma,1.0,1.0,0.2)  );
        lista.add( new Jogo(2,1.0,3.0,EOperator.soma,2.0,2.0,0.6)  );
        
        Partida partida = new Partida();
        partida.setJogoList(lista);
        
       int esperado = 2;
       
       int resultado = partida.getAcertos();
       
        assertEquals(esperado, resultado);
    }
    
    @Test
    @DisplayName("Verificar erros")
    public void testGetErros() {
         ArrayList<Jogo> lista = new ArrayList<>();
        lista.add( new Jogo(1,1.0,1.0,EOperator.soma,1.0,1.3,0.2)  );
        lista.add( new Jogo(2,1.0,3.0,EOperator.soma,2.0,2.0,0.6)  );
        
        Partida partida = new Partida();
        partida.setJogoList(lista);
        
       int esperado = 1;
       
       int resultado = partida.getErros();
       
        assertEquals(esperado, resultado);
    }
    
    @Test
    @DisplayName("Teste formatar data")
    public void testGetDataFormatada() {
        LocalDateTime DATE = LocalDateTime.of(2017, 2, 13, 15, 56);  
        Partida partida = new Partida(1, DATE,0.2,200);
        
        String esperado = "13/02/2017 03:56:00";
        
       String resultado =  partida.getDataFormatada();
       
        assertEquals(esperado, resultado);
    }
    
    
}
