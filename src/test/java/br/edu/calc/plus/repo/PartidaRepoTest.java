/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.calc.plus.repo;

import br.edu.calc.plus.domain.Partida;
import br.edu.calc.plus.domain.Usuario;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.MatcherAssert.assertThat;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.DisplayName;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;

/**
 *
 * @author danie
 */
@DataJpaTest
@ActiveProfiles("test")
public class PartidaRepoTest {

    public PartidaRepoTest() {
    }

    @Autowired
    private PartidaRepo partidaRepo;

    @Autowired
    private UsuarioRepo userRepo;

    private LocalDateTime dtIni = LocalDate.now().atStartOfDay();
    private LocalDateTime dtFim = LocalDate.now().atTime(23, 59, 59);
    private Partida partida = new Partida(1, dtIni, 0.2, 30);

    @BeforeAll
    public static void setUpClass() {
    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {
        Usuario u = userRepo.findByLogin("daves").get();
        partida.setUsuario(u);
        partidaRepo.save(partida);
    }

    @AfterEach
    public void tearDown() {
        partidaRepo.deleteAll();
    }

    @Test
    @DisplayName("Verificar se retornou a quantidade correta de partidas")
    public void testFindByUserId() {
        Usuario u = userRepo.findByLogin("daves").get();
        ArrayList<Partida> resultado = (ArrayList<Partida>) partidaRepo.findByUsuarioId(u.getId());

        assertEquals(1, resultado.size());
    }

    @Test
    @DisplayName("Verificar se usuario ja competiu")
    public void testGetUserJaCompetiu() {
        Usuario u = userRepo.findByLogin("daves").get();
        Long resultado = partidaRepo.getUsuarioCompetil(u.getId(), dtIni, dtFim);

        assertEquals(1, resultado);
    }

    @Test
    @DisplayName("Verificar se usuario invalido ja competiu")
    public void testGetUserJaCompetiuInvalido() {
        Long resultado = partidaRepo.getUsuarioCompetil(-1, dtIni, dtFim);

        assertEquals(0, resultado);
    }

}
