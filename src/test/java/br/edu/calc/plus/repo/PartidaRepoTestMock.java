/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.calc.plus.repo;

import br.edu.calc.plus.domain.Partida;
import br.edu.calc.plus.domain.Usuario;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

/**
 *
 * @author danie
 */
@ActiveProfiles("test")
@ExtendWith(SpringExtension.class)
public class PartidaRepoTestMock {

    public PartidaRepoTestMock() {
        this.user = new Usuario(1, "ze", "ze", "ze@ze.com", "1234", "jf", LocalDate.of(2017, 2, 13));

    }

    @MockBean
    private PartidaRepo partidaRepo;

    private Usuario user;

    @BeforeAll
    public static void setUpClass() {
    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @AfterEach
    public void tearDown() {
    }

    @Test
    @DisplayName("Teste findByUsarioId existente")
    public void testFindByUsuarioId() {
        ArrayList<Partida> lista = new ArrayList<>();
        Partida partida = new Partida(1);
        partida.setUsuario(user);
        lista.add(partida);

        Mockito.when(partidaRepo.findByUsuarioId(1)).thenReturn(lista);

        List<Partida> resultado = partidaRepo.findByUsuarioId(1);

        assertEquals(lista,resultado );

    }

    @Test
    @DisplayName("Teste findByUsarioId inexistente")
    public void testFindByUsuarioIdErro() {
        ArrayList<Partida> lista = new ArrayList<>();
        Partida partida = new Partida(1);
        partida.setUsuario(user);
        lista.add(partida);

        Mockito.when(partidaRepo.findByUsuarioId(-1)).thenReturn(null);

        List<Partida> resultado = partidaRepo.findByUsuarioId(-1);

        assertEquals(null,resultado );

    }
    
        @Test
        @DisplayName("Teste se usuario ja competiu mock")
        public void getUsuarioCompetiuMock() {
        ArrayList<Partida> lista = new ArrayList<>();
        Partida partida = new Partida(1);
        partida.setUsuario(user);
        lista.add(partida);
        
        LocalDateTime dtIni = LocalDate.now().atStartOfDay();
	LocalDateTime dtFim = LocalDate.now().atTime(23, 59, 59);

        Mockito.when(partidaRepo.getUsuarioCompetil(1, dtIni, dtFim)).thenReturn(1L);

        Long resultado = partidaRepo.getUsuarioCompetil(1, dtIni, dtFim);

        assertEquals(1,resultado);

    }

}
