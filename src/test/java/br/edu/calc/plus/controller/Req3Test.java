/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.calc.plus.controller;

import br.edu.calc.plus.repo.UsuarioRepo;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ActiveProfiles;

/**
 *
 * @author danie
 */
@ActiveProfiles("test")
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class Req3Test {
    
    @Autowired
    UsuarioRepo userRepo;

    private WebDriver driver;
    private static final String URL = "http://localhost:9090/login";

    @BeforeEach
    public void setUp() {
        System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
        driver = new ChromeDriver();

        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        try {
            Thread.sleep(3500);
        } catch (InterruptedException ex) {
            Logger.getLogger(Req5Test.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @AfterEach
    public void tearDown() {
        driver.quit();        
    }
    
    @Test
    public void testQuantiadadeDeUsuariosCadastrados() {
        driver.get(URL);
        driver.manage().window().setSize(new Dimension(1324, 866));       
        driver.findElement(By.id("username")).sendKeys("daves");
        driver.findElement(By.id("password")).sendKeys("123456");
        driver.findElement(By.cssSelector(".btn-primary")).click();   

        assertThat(driver.findElement(By.className("card-title")).getText(), is("1"));
    }
    
    @Test
    public void testTotalDeErros() {
        driver.get(URL);
        driver.manage().window().setSize(new Dimension(1324, 866));       
        driver.findElement(By.id("username")).sendKeys("daves");
        driver.findElement(By.id("password")).sendKeys("123456");
        driver.findElement(By.cssSelector(".btn-primary")).click();   

        assertThat(driver.findElement(By.className("card-title")).getText(), is("30"));
    }
    
}
